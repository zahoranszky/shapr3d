package shapr3d.converter.service;

import io.micronaut.configuration.kafka.annotation.KafkaKey;
import io.micronaut.configuration.kafka.annotation.KafkaListener;
import io.micronaut.configuration.kafka.annotation.Topic;
import lombok.extern.slf4j.Slf4j;
import shapr3d.api.ConversionMessage;

@Slf4j
@KafkaListener(value = "${kafka-consumer-group}")
public class ConversionStartedEventListener {

    private final FileConverter fileConverter;

    public ConversionStartedEventListener(FileConverter fileConverter) {
        this.fileConverter = fileConverter;
    }

    @Topic("conversionStarted")
    public void receive(@KafkaKey String id, ConversionMessage message) {
        log.info("Message received: {}", message);
        fileConverter.convert(message.getId(), message.getOriginalContent(), message.getTargetFormat());
    }

}