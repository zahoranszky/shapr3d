package shapr3d.converter.service.converter;

import io.micronaut.context.annotation.Property;

import javax.inject.Singleton;

@Singleton
public class ObjConverter extends AbstractShapr3dConverter {

    public ObjConverter(@Property(name = "converters.obj.max-sleep-time") long maxSleepTime) {
        super("obj", maxSleepTime);
    }

}
