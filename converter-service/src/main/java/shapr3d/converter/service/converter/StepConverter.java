package shapr3d.converter.service.converter;

import io.micronaut.context.annotation.Property;

import javax.inject.Singleton;

@Singleton
public class StepConverter extends AbstractShapr3dConverter {

    public StepConverter(@Property(name = "converters.step.max-sleep-time") long maxSleepTime) {
        super("step", maxSleepTime);
    }

}
