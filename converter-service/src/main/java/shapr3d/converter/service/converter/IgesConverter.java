package shapr3d.converter.service.converter;

import io.micronaut.context.annotation.Property;

import javax.inject.Singleton;

@Singleton
public class IgesConverter extends AbstractShapr3dConverter {

    public IgesConverter(@Property(name = "converters.iges.max-sleep-time") long maxSleepTime) {
        super("iges", maxSleepTime);
    }

}
