package shapr3d.converter.service.converter;

import io.micronaut.context.annotation.Property;

import javax.inject.Singleton;

@Singleton
public class StlConverter extends AbstractShapr3dConverter {

    public StlConverter(@Property(name = "converters.stl.max-sleep-time") long maxSleepTime) {
        super("stl", maxSleepTime);
    }

}
