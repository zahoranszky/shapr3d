package shapr3d.converter.service.converter;

import io.micronaut.context.annotation.Property;
import shapr3d.converter.service.Feedback;
import shapr3d.converter.service.Shapr3dConverter;

import javax.inject.Singleton;
import java.util.Random;

@Singleton
public class AbstractShapr3dConverter implements Shapr3dConverter {

    protected final long maxSleepTime;
    private final String name;

    public AbstractShapr3dConverter(String name, long maxSleepTime) {
        this.maxSleepTime = maxSleepTime;
        this.name = name;
    }

    @Override
    public String convert(String content, Feedback feedback) {
        for (int i = 10; i <= 100; i += 10) {
            try {
                Thread.sleep(Math.abs(new Random().nextLong()) % maxSleepTime);
                feedback.percent(i);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
        return content.toUpperCase() + " in " + name + " format";
    }

}
