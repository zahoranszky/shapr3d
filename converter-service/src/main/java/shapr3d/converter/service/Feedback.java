package shapr3d.converter.service;

public interface Feedback {

    void percent(int percent);

}
