package shapr3d.converter.service;

import io.micronaut.configuration.kafka.annotation.KafkaClient;
import io.micronaut.configuration.kafka.annotation.KafkaKey;
import io.micronaut.configuration.kafka.annotation.Topic;
import shapr3d.api.ConversionMessage;

@KafkaClient
public interface ConversionCompleteEventPublisher {

    @Topic("conversionComplete")
    void conversionComplete(@KafkaKey String id, ConversionMessage message);

}
