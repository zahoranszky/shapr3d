package shapr3d.converter.service;

import lombok.extern.slf4j.Slf4j;
import shapr3d.api.ConversionMessage;
import shapr3d.api.TargetFormat;

import java.util.Map;
import java.util.concurrent.ExecutorService;

@Slf4j
public class FileConverter {

    private final ConversionProgressEventPublisher conversionProgressEventPublisher;
    private final ConversionCompleteEventPublisher conversionCompleteEventPublisher;
    private final ExecutorService executorService;
    private final Map<TargetFormat, Shapr3dConverter> converters;

    public FileConverter(ConversionProgressEventPublisher conversionProgressEventPublisher,
                         ConversionCompleteEventPublisher conversionCompleteEventPublisher,
                         ExecutorService executorService,
                         Map<TargetFormat, Shapr3dConverter> converters) {
        this.conversionProgressEventPublisher = conversionProgressEventPublisher;
        this.conversionCompleteEventPublisher = conversionCompleteEventPublisher;
        this.executorService = executorService;
        this.converters = converters;
    }

    public void convert(String id, String content, TargetFormat targetFormat) {
        executorService.execute(() -> {
            String convertedContent = converters.get(targetFormat).convert(content, percent -> {
                conversionProgressEventPublisher.conversionProgress(id,
                    ConversionMessage.builder()
                        .id(id)
                        .targetFormat(targetFormat)
                        .percent(percent)
                        .build());
                log.info("File conversion for id {} is at {}%", id, percent);
            });
            conversionCompleteEventPublisher.conversionComplete(id,
                ConversionMessage.builder()
                    .id(id)
                    .targetFormat(targetFormat)
                    .percent(100)
                    .convertedContent(convertedContent)
                    .build());
        });
    }

}
