package shapr3d.converter.service;

public interface Shapr3dConverter {

    String convert(String content, Feedback feedback);

}
