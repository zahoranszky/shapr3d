package shapr3d.converter.config;

import io.micronaut.context.annotation.Factory;
import lombok.extern.slf4j.Slf4j;
import shapr3d.api.TargetFormat;
import shapr3d.converter.service.ConversionCompleteEventPublisher;
import shapr3d.converter.service.ConversionProgressEventPublisher;
import shapr3d.converter.service.FileConverter;
import shapr3d.converter.service.Shapr3dConverter;
import shapr3d.converter.service.converter.IgesConverter;
import shapr3d.converter.service.converter.ObjConverter;
import shapr3d.converter.service.converter.StepConverter;
import shapr3d.converter.service.converter.StlConverter;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

@Slf4j
@Factory
public class BeanFactory {

    @Singleton
    public FileConverter fileConverter(ConversionProgressEventPublisher conversionProgressEventPublisher,
                                       ConversionCompleteEventPublisher conversionCompleteEventPublisher,
                                       ExecutorService executorService,
                                       IgesConverter igesConverter,
                                       StepConverter stepConverter,
                                       ObjConverter objConverter,
                                       StlConverter stlConverter) {
        HashMap<TargetFormat, Shapr3dConverter> converters =  new HashMap<>();
        converters.put(TargetFormat.iges, igesConverter);
        converters.put(TargetFormat.step, stepConverter);
        converters.put(TargetFormat.obj, objConverter);
        converters.put(TargetFormat.stl, stlConverter);

        return new FileConverter(conversionProgressEventPublisher, conversionCompleteEventPublisher, executorService, converters);
    }

}
