package shapr3d.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import shapr3d.api.ConversionMessage;
import shapr3d.api.TargetFormat;
import shapr3d.converter.service.ConversionCompleteEventPublisher;
import shapr3d.converter.service.ConversionProgressEventPublisher;
import shapr3d.converter.service.FileConverter;
import shapr3d.converter.service.Shapr3dConverter;
import shapr3d.converter.service.converter.IgesConverter;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;

@ExtendWith(MockitoExtension.class)
public class FileConverterTest {

    private FileConverter fileConverter;

    @Mock
    private ConversionProgressEventPublisher conversionProgressEventPublisherMock;

    @Mock
    private ConversionCompleteEventPublisher conversionCompleteEventPublisherMock;

    @Mock
    private ExecutorService executorServiceMock;

    @BeforeEach
    public void beforeAll() {
        HashMap<TargetFormat, Shapr3dConverter> converters =  new HashMap<>();
        converters.put(TargetFormat.iges, new IgesConverter(1   ));

        fileConverter = new FileConverter(
            conversionProgressEventPublisherMock,
            conversionCompleteEventPublisherMock,
            executorServiceMock,
            converters
        );
    }

    @Test
    public void testConversion() throws InterruptedException {
        // Given
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Runnable r = invocation.getArgument(0);
                r.run();
                return null;
            }
        }).when(executorServiceMock).execute(ArgumentMatchers.any());

        // When
        fileConverter.convert("id", "hello", TargetFormat.iges);

        // Then
        Mockito.verify(conversionCompleteEventPublisherMock).conversionComplete("id",
            ConversionMessage.builder()
                .id("id")
                .targetFormat(TargetFormat.iges)
                .percent(100)
                .convertedContent("HELLO in iges format")
                .build());
    }

}
