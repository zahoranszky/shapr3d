conn = new Mongo();
db = conn.getDB("files");
db.createCollection("files")

db.createUser(
    {
        user: "root",
        pwd: "password",
        roles: [
            {
                role: "readWrite",
                db: "files"
            }
        ]
    }
);