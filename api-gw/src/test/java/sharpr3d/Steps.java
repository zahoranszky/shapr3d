package sharpr3d;

import feign.Feign;
import feign.Response;
import feign.form.FormEncoder;
import feign.jackson.JacksonEncoder;
import io.cucumber.java.Before;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;

public class Steps {

    private TestRestClient testRestClient;
    private String conversionId;

    @Before
    public void setup() throws InterruptedException {
        this.testRestClient = Feign.builder()
            .encoder(new FormEncoder(new JacksonEncoder()))
            .target(TestRestClient.class, "http://localhost:8080");

        // Wait for startup. Checking some health endpoint would be more robust
        Thread.sleep(10000);
    }

    @When("the REST api is called to convert a file to format {string}")
    public void restAPIisCallewd(String targetFormat) throws IOException {
        File tempFile = File.createTempFile("fileToUpload", "temp");
        Response res = testRestClient.upload("fileToUpload", tempFile, "iges");
        this.conversionId = res.body().toString();
    }

    @Then("we get back an id for the conversion")
    public void getBackAnId() throws InterruptedException {
        Thread.sleep(1000);
    }

    @Then("we can download the original file")
    public void getTheOriginalFile() {
        Response res = testRestClient.original(conversionId);
        Assert.assertEquals(200, res.status());
    }

}
