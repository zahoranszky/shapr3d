package sharpr3d;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import feign.Response;

import java.io.File;

public interface TestRestClient {

    @RequestLine("POST /api/v1/upload?targetFormat={targetFormat}")
    @Headers("Content-Type: multipart/form-data")
    Response upload(@Param("name") String name, @Param("file") File file, @Param("targetFormat") String targetFormat);

    @RequestLine("GET /api/v1/downloads/{id}/original")
    Response original(@Param("id") String id);

}
