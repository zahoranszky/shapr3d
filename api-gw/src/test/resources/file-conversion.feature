Feature: Conversion feature

  Scenario: A conversion request is sent via the REST API. After that we can download the original file.
    When the REST api is called to convert a file to format "iges"
    Then we get back an id for the conversion
    Then we can download the original file
