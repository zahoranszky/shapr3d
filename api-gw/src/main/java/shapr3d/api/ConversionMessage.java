package shapr3d.api;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class ConversionMessage {

    private String id;
    private String originalFileName;
    private String originalContent;
    private TargetFormat targetFormat;
    private String convertedContent;
    private int percent;

}
