package shapr3d.gw.config;

import feign.Feign;
import feign.gson.GsonDecoder;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Property;
import shapr3d.gw.controller.DownloadRestClient;

import javax.inject.Singleton;

@Factory
public class BeanFactory {

    @Singleton
    public DownloadRestClient downloadRestClient(@Property(name = "download-service-url") String downloadServiceUrl) {
        return Feign.builder()
            .decoder(new GsonDecoder())
            .target(DownloadRestClient.class, downloadServiceUrl);
    }

}
