package shapr3d.gw.controller;

import feign.Response;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.HttpStatus;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Error;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.annotation.QueryValue;
import io.micronaut.http.hateoas.JsonError;
import io.micronaut.http.multipart.StreamingFileUpload;
import io.micronaut.http.server.types.files.SystemFile;
import io.reactivex.Single;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import shapr3d.api.ConversionMessage;
import shapr3d.api.ConversionResponse;
import shapr3d.api.ConversionStatus;
import shapr3d.api.TargetFormat;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

@Slf4j
@Controller("/api/v1")
public class GatewayController {

    private final EventPublisher eventPublisher;
    private final DownloadRestClient downloadRestClient;

    public GatewayController(EventPublisher eventPublisher, DownloadRestClient downloadRestClient) {
        this.eventPublisher = eventPublisher;
        this.downloadRestClient = downloadRestClient;
    }

    @Post(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA)
    public Single<HttpResponse<ConversionResponse>> upload(StreamingFileUpload file, @QueryValue("targetFormat") String targetFormat) throws IOException {
        File tempFile = File.createTempFile(file.getFilename(), "temp");
        Publisher<Boolean> uploadPublisher = file.transferTo(tempFile);
        return Single.fromPublisher(uploadPublisher)
            .map(success -> {
                if (success) {
                    ConversionMessage message = ConversionMessage.builder()
                        .id(UUID.randomUUID().toString())
                        .originalFileName(file.getFilename())
                        .targetFormat(TargetFormat.valueOf(targetFormat))
                        .originalContent(Files.readString(Paths.get(tempFile.getPath())))
                        .build();
                    eventPublisher.sendFilesToConvert(message.getId(), message);
                    log.info("Message sent {}", message);
                    return HttpResponse.ok(ConversionResponse.builder().id(message.getId()).build());
                } else {
                    return HttpResponse.<String>status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .body(ConversionResponse.builder().build());
                }
            });
    }

    @Get("/downloads/{id}/original")
    public SystemFile original(@PathVariable("id") String id) throws IOException {
        return responseToSystemFile(downloadRestClient.original(id));
    }

    @Get("/downloads/{id}/converted")
    public SystemFile converted(@PathVariable("id") String id) throws IOException {
        return responseToSystemFile(downloadRestClient.converted(id));
    }

    @Get("/downloads")
    public List<ConversionStatus> statuses() {
        return downloadRestClient.statuses();
    }

    @Error(exception = IllegalArgumentException.class)
    public HttpResponse<JsonError> error() {
        JsonError error = new JsonError("Invalid request Oooops");
        return HttpResponse.<JsonError>status(HttpStatus.BAD_REQUEST).body(error);
    }

    private SystemFile responseToSystemFile(Response response) throws IOException {
        try (InputStream is = response.body().asInputStream()) {
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            File tempFile = File.createTempFile("original", "temp");
            Files.write(Paths.get(tempFile.getPath()), buffer);
            return new SystemFile(tempFile, MediaType.TEXT_PLAIN_TYPE);
        }
    }

}
