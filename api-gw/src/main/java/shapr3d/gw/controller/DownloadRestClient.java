package shapr3d.gw.controller;

import feign.Param;
import feign.RequestLine;
import feign.Response;
import shapr3d.api.ConversionStatus;

import java.util.List;

public interface DownloadRestClient {

    @RequestLine("GET /downloads/{id}/original")
    Response original(@Param("id") String id);

    @RequestLine("GET /downloads/{id}/converted")
    Response converted(@Param("id") String id);

    @RequestLine("GET /downloads")
    List<ConversionStatus> statuses();

}
