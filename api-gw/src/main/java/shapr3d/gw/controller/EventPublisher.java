package shapr3d.gw.controller;

import io.micronaut.configuration.kafka.annotation.KafkaClient;
import io.micronaut.configuration.kafka.annotation.KafkaKey;
import io.micronaut.configuration.kafka.annotation.Topic;
import shapr3d.api.ConversionMessage;

@KafkaClient
public interface EventPublisher {

    @Topic("conversionStarted")
    void sendFilesToConvert(@KafkaKey String id, ConversionMessage conversionMessage);

}
