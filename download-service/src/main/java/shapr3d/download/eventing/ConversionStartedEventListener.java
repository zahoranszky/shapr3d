package shapr3d.download.eventing;

import shapr3d.api.ConversionMessage;
import shapr3d.download.model.Conversion;
import shapr3d.download.repository.ConversionRepo;
import io.micronaut.configuration.kafka.annotation.KafkaKey;
import io.micronaut.configuration.kafka.annotation.KafkaListener;
import io.micronaut.configuration.kafka.annotation.OffsetReset;
import io.micronaut.configuration.kafka.annotation.Topic;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@KafkaListener(value = "${kafka-consumer-group}")
public class ConversionStartedEventListener {

    private final ConversionRepo conversionRepo;

    public ConversionStartedEventListener(ConversionRepo conversionRepo) {
        this.conversionRepo = conversionRepo;
    }

    @Topic("conversionStarted")
    public void receive(@KafkaKey String id, ConversionMessage message) {
        log.info("Conversion started event received {}", message);
        Conversion conversion = Conversion.builder()
            .id(id)
            .originalContent(message.getOriginalContent())
            .originalFileName(message.getOriginalFileName())
            .targetFormat(message.getTargetFormat())
            .percent(0)
            .build();
        conversionRepo.upsert(conversion);
    }

}