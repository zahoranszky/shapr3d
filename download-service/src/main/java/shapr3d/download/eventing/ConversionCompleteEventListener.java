package shapr3d.download.eventing;

import shapr3d.api.ConversionMessage;
import shapr3d.download.model.Conversion;
import shapr3d.download.repository.ConversionRepo;
import io.micronaut.configuration.kafka.annotation.KafkaKey;
import io.micronaut.configuration.kafka.annotation.KafkaListener;
import io.micronaut.configuration.kafka.annotation.OffsetReset;
import io.micronaut.configuration.kafka.annotation.Topic;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@KafkaListener(value = "${kafka-consumer-group}")
public class ConversionCompleteEventListener {

    private final ConversionRepo conversionRepo;

    public ConversionCompleteEventListener(ConversionRepo conversionRepo) {
        this.conversionRepo = conversionRepo;
    }

    @Topic("conversionComplete")
    public void receive(@KafkaKey String id, ConversionMessage message) {
        log.info("Conversion completed event received {}", message);
        Conversion conversion = Conversion.builder()
            .id(id)
            .targetFormat(message.getTargetFormat())
            .percent(100)
            .convertedContent(message.getConvertedContent())
            .error(null)
            .build();
        conversionRepo.upsert(conversion);
    }

}