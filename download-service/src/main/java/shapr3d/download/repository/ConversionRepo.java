package shapr3d.download.repository;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import shapr3d.api.TargetFormat;
import shapr3d.download.model.Conversion;

import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.ne;
import static com.mongodb.client.model.Updates.set;

@Slf4j
@Singleton
public class ConversionRepo {

    public static final String OBJECT_ID = "_id";
    public static final String LAST_UPDATED_TIME_STAMP = "lastUpdatedTimeStamp";
    public static final String ORIGINAL_FILE_NAME = "originalFileName";
    public static final String ORIGINAL_CONTENT = "originalContent";
    public static final String TARGET_FORMAT = "targetFormat";
    public static final String PERCENT = "percent";
    public static final String CONVERTED_CONTENT = "convertedContent";
    public static final String ERROR = "error";

    private final MongoCollection<Document> collection;

    public ConversionRepo(MongoClient mongoClient) {
        MongoDatabase db = mongoClient.getDatabase("files");
        collection = db.getCollection("files");
    }

    public Optional<Conversion> get(String id) {
        Document document = collection.find(eq(OBJECT_ID, id)).first();
        if (null == document) {
            return Optional.empty();
        } else {
            return Optional.of(createConversion(document));
        }
    }

    public List<Conversion> getAll() {
        List ret = new ArrayList();
        FindIterable<Document> r = collection.find();
        for (Document document:collection.find()) {
            ret.add(createConversion(document));
        }
        return ret;
    }

    public void upsert(Conversion conversion) {
        Document oldDocument = collection.find(eq(OBJECT_ID, conversion.getId())).first();

        Document newDocument = null;
        if (null != oldDocument) {
            newDocument = createUpdatedDocument(oldDocument, conversion);
            collection.updateOne(eq(OBJECT_ID, oldDocument.get(OBJECT_ID)), new Document("$set", newDocument));
            log.info("Conversion has been updated: {}", conversion);

        } else {
            newDocument = createNewDocument(conversion);
            collection.insertOne(newDocument);
            log.info("New conversion stored: {}", conversion);
        }
    }

    public void setErrorForOldConversions(long olderThan, String errorMessage) {
        collection.updateMany(and(lt(LAST_UPDATED_TIME_STAMP, olderThan), ne(PERCENT, 100)), set(ERROR, errorMessage));
    }

    private Conversion createConversion(Document document) {
        return Conversion.builder()
            .id((String) document.get(OBJECT_ID))
            .originalFileName((String) document.get(ORIGINAL_FILE_NAME))
            .originalContent((String) document.get(ORIGINAL_CONTENT))
            .targetFormat(TargetFormat.valueOf((String) document.get(TARGET_FORMAT)))
            .percent((Integer) document.get(PERCENT))
            .convertedContent((String) document.get(CONVERTED_CONTENT))
            .error((String) document.get(ERROR))
            .build();
    }

    private Document createNewDocument(Conversion conversion) {
        Document document = new Document();
        document.append(OBJECT_ID, conversion.getId());
        document.append(LAST_UPDATED_TIME_STAMP, System.currentTimeMillis());
        document.append(ORIGINAL_FILE_NAME, conversion.getOriginalFileName());
        document.append(ORIGINAL_CONTENT, conversion.getOriginalContent());
        document.append(TARGET_FORMAT, conversion.getTargetFormat().toString());
        document.append(PERCENT, conversion.getPercent());
        document.append(CONVERTED_CONTENT, conversion.getConvertedContent());
        document.append(ERROR, conversion.getError());
        return document;
    }

    private Document createUpdatedDocument(Document oldDocument, Conversion conversion) {
        Document document = new Document();
        document.append(OBJECT_ID, conversion.getId());
        document.append(LAST_UPDATED_TIME_STAMP, System.currentTimeMillis());
        document.append(ORIGINAL_FILE_NAME, oldDocument.get(ORIGINAL_FILE_NAME));
        document.append(ORIGINAL_CONTENT, oldDocument.get(ORIGINAL_CONTENT));
        document.append(TARGET_FORMAT, oldDocument.get(TARGET_FORMAT));
        document.append(PERCENT, conversion.getPercent() > (int) oldDocument.get(PERCENT)
            ? conversion.getPercent()
            : oldDocument.get(PERCENT));
        document.append(CONVERTED_CONTENT, conversion.getConvertedContent());
        document.append(ERROR, conversion.getError());
        return document;
    }

}
