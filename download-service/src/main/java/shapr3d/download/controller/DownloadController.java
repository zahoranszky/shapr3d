package shapr3d.download.controller;

import shapr3d.api.ConversionStatus;
import shapr3d.api.Status;
import shapr3d.download.model.Conversion;
import shapr3d.download.repository.ConversionRepo;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.server.types.files.SystemFile;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Controller("/downloads")
public class DownloadController {

    private final ConversionRepo conversionRepo;

    public DownloadController(ConversionRepo conversionRepo) {
        this.conversionRepo = conversionRepo;
    }

    @Get("/{id}/original")
    public SystemFile download(@PathVariable("id") String id) throws IOException {
        Optional<Conversion> oConversion = conversionRepo.get(id);
        Conversion conversion = oConversion.orElseThrow();
        File tempFile = File.createTempFile("original", "temp");
        Files.writeString(Paths.get(tempFile.getPath()), conversion.getOriginalContent());
        return new SystemFile(tempFile, MediaType.TEXT_PLAIN_TYPE);
    }

    @Get("/{id}/converted")
    public SystemFile converted(@PathVariable("id") String id) throws IOException {
        Optional<Conversion> oConversion = conversionRepo.get(id);
        Conversion conversion = oConversion.orElseThrow();
        File tempFile = File.createTempFile("converted", "temp");
        Files.writeString(Paths.get(tempFile.getPath()), conversion.getConvertedContent());
        return new SystemFile(tempFile, MediaType.TEXT_PLAIN_TYPE);
    }

    @Get("/")
    public List<ConversionStatus> statuses() {
        List<ConversionStatus> ret = new ArrayList<>();
        for (Conversion conversion: conversionRepo.getAll()) {
            ret.add(ConversionStatus.builder()
                .id(conversion.getId())
                .percent(conversion.getPercent())
                .status(calculateStatus(conversion))
                .build());
        }
        return ret;
    }

    private Status calculateStatus(Conversion conversion) {
        if (null != conversion.getError()) {
            return Status.failed;
        } else if (conversion.getPercent() == 0) {
            return Status.waiting;
        } else if (conversion.getPercent() > 0 && conversion.getPercent() < 100) {
            return Status.in_progress;
        } else {
            return Status.completed;
        }
    }

}
