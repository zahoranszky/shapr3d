package shapr3d.download.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import shapr3d.api.TargetFormat;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Conversion {

    private String id;
    private String originalFileName;
    private String originalContent;
    private TargetFormat targetFormat;
    private String convertedContent;
    private int percent;
    private String error;

}
