package shapr3d.api;

public enum Status {

    waiting,
    in_progress,
    completed,
    failed

}
