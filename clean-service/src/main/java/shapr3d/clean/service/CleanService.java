package shapr3d.clean.service;

import io.micronaut.context.annotation.Property;
import io.micronaut.scheduling.annotation.Scheduled;
import lombok.extern.slf4j.Slf4j;
import shapr3d.clean.repository.ConversionRepo;

import javax.inject.Singleton;

@Slf4j
@Singleton
public class CleanService {

    private final ConversionRepo conversionRepo;
    private final long oldConversionAge;

    public CleanService(ConversionRepo conversionRepo,
                        @Property(name = "old-conversion-age") long oldConversionAge) {
        this.conversionRepo = conversionRepo;
        this.oldConversionAge = oldConversionAge;
    }

    @Scheduled(fixedRate = "10s")
    public void clean() {
        long now = System.currentTimeMillis();
        long olderThan = now - oldConversionAge;
        log.info("Cleaning conversions older than {}", olderThan);
        conversionRepo.setErrorForOldConversions(olderThan, "timeout");
    }

}
