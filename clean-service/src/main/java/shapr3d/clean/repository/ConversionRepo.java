package shapr3d.clean.repository;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;

import javax.inject.Singleton;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.ne;
import static com.mongodb.client.model.Updates.set;

@Slf4j
@Singleton
public class ConversionRepo {

    public static final String LAST_UPDATED_TIME_STAMP = "lastUpdatedTimeStamp";
    public static final String PERCENT = "percent";
    public static final String ERROR = "error";

    private final MongoCollection<Document> collection;

    public ConversionRepo(MongoClient mongoClient) {
        MongoDatabase db = mongoClient.getDatabase("files");
        collection = db.getCollection("files");
    }

    public void setErrorForOldConversions(long olderThan, String errorMessage) {
        collection.updateMany(and(lt(LAST_UPDATED_TIME_STAMP, olderThan), ne(PERCENT, 100)), set(ERROR, errorMessage));
    }

}
