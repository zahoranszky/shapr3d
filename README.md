# Shapr3d file conversion application #

## Specification ##

[ Description ](https://docs.google.com/document/d/1soIXJXN8dJCV-c7NsglfAXFo3rJ4A6ortm2tvRsbvJM/edit#)

## Architecture ##

The design is based on event driven microservice architecture over the Micronaut Java framework. 

![Architecture](doc/Architecture.png)

Components

* Frontend: NodeJS implementation of the web GUI
* API Gateway: as its name shows it is an API gateway which hides the infrastructure for the 
  frontend application and provides a simple uniform REST API
* Converter service: this service does the file conversion itself 
* Download service: receives events about the conversion (conversion started, conversion progress, 
  conversion completed) from other services (from API gateway and conversion service) and updates the domain 
  model according to them. It can be interpreted as [ CQRS ](https://martinfowler.com/bliki/CQRS.html) since the 
  service consumes asyn events and updates the database which is used to generate queries describing the actual 
  status of the conversion.
* Clean service: some conversion can timeout because of various reasons. This service periodically 
  checks the underlying database and sets conversion status to "timeout" if the db record hasn't 
  been updated for a certain time. Each record in the database has a timestamp which indicates the last 
  update time.
* Messaging fw: used for internal messaging. In this implementation it is Kafka but in the cloud 
  AWS Kinesis can be used
* Conversion store: stores the state of the individual conversions. In this implementation the underlying 
  DB is MongoDB however in the cloud it can be DynamoDB 

### Converter module design ###

I only have detailed design for the Converter module because the other modules are quite simple and 
straightforward. 

![Architecture](doc/Class-Converter.png)

The Converter Micronaut Applications is the entry point of the software. It uses a Kafka event listener called 
ConversionStartedEventListener which receives async events from the API Gateway module. The received 
message is passed to the file converter which does the actual conversion asynchronously. It has 
Shapr3dConverter instances for the individual target formats (.step, .obj, etc.). Each converter 
gets a Feedback instance which is used to send feedback about the progress of the conversion. The 
FileConverter sends the progress and complete events to the Download module via the ConversionProgressEventPublisher 
and ConversionCompleteEventPublisher Kafka producers. 

## How to run ##

```
> git clone git@bitbucket.org:zahoranszky/shapr3d.git
> cd shapr3d 
> ./gradlew startApp
> echo hello > example.shapr
```

Wait a bit until the application starts.

```
> curl -F 'file=@example.shapr' 'http://localhost:8080/api/v1/upload?targetFormat=iges'
{"id":"5584c08d-5eee-4b75-b7a3-964d283ca17c"}
> curl 'http://localhost:8080/api/v1/downloads/5584c08d-5eee-4b75-b7a3-964d283ca17c/original' -o -
hello
> curl 'http://localhost:8080/api/v1/downloads'     
[{"id":"5584c08d-5eee-4b75-b7a3-964d283ca17c","status":"completed","percent":100}]
```

Repeat the last step until the status is 'completed'. It will take max 30 sec according the default 
configuration (the converter simulates slow conversion). See 
[ MAX_SLEEP_TIME ](https://bitbucket.org/zahoranszky/shapr3d/src/cd811550a06aa52ce375f5b80715a18047e6fec1/docker-compose.yml#lines-31): 
30 sec = MAX_SLEEP_TIME * 10
 
```
> curl 'http://localhost:8080/api/v1/downloads/5584c08d-5eee-4b75-b7a3-964d283ca17c/converted' -o -
HELLO in iges format

> ./gradlew shutdownApp
```

## Runing e2e BDD (Cucumber) tests ##

I have implemeted only one BDD test using gherkin DSL as an appetizer. You can run it this way

```
./gradlew cucumber
```

## Deployment model ##

![Architecture](doc/Deployment.png)

The implementation is based on docker containers because development time it is easier to mange. 
However in the cloud I prefer managed services since they provide some features out of the box 
(autoscaling, load balancing, etc.). Using them during development is a bit more complicated since 
 many of them cannot be easily run locally. In that case many mock services should have been implemented 
 and I didn't have time for that. In AWS DynamoDB, AWS Beanstalk, Lambda and Kinesis streams are used.

## Scaling ##

AWS Elastic beanstalk provides auto-scaling and load balancing capability out of the box. The 
Converter service is deployed as an AWS elastic beanstalk application. 

I would have used Kubernetes also. It is another alternative.

## Testing ##

The test coverage is quite poor because of the lack of time. However I have written one 2e2 test 
in BDD (Cucumber).

```
Feature: Conversion feature

  Scenario: A conversion request is sent via the REST API. After that we can download the original file.
    When the REST api is called to convert a file to format "iges"
    Then we get back an id for the conversion
    Then we can download the original file
```

See [BDD test](https://bitbucket.org/zahoranszky/shapr3d/src/master/api-gw/src/test/resources/file-conversion.feature)

I haven't implemented integration test because I didn't have enough time. However the Micronaut fw 
provdes quite advanced means for that (see [ Testing ](https://micronaut-projects.github.io/micronaut-test/latest/guide/index.html)).

I have writen only one unit test also because of the lack of time.

See [Unit test](https://bitbucket.org/zahoranszky/shapr3d/src/master/converter-service/src/test/java/shapr3d/converter/FileConverterTest.java)

## API ##

The API could have been documented via OpenAPI but for now I just simple describe the API:

#### upload ####

```
curl -F 'file=@example.shapr' 'http://localhost:8080/api/v1/upload?targetFormat=iges'
{"id":"75fc46a6-5b2a-4ac9-bb43-002d955e9467"}
```

#### download original file ####

```
curl 'http://localhost:8080/api/v1/downloads/75fc46a6-5b2a-4ac9-bb43-002d955e9467/original' 
hello 
```

#### download converted file ####

```
curl 'http://localhost:8080/api/v1/downloads/75fc46a6-5b2a-4ac9-bb43-002d955e9467/converted'  
HELLO in iges format
```

#### info ####

```
curl 'http://localhost:8080/api/v1/downloads'
[
  {
    "id": "8ef5588b-e3d2-4c7b-849a-e538969e725a",
    "status": "in_progress",
    "percent": 50
  },
  {
    "id": "4aff457a-2a80-44f3-972d-1ba153c2d36b",
    "status": "waiting",
    "percent": 0
  }
]
```

## Future considerations ##

I would have done much more of course like

* Using [ Envoy proxy ](https://www.envoyproxy.io/) to make the application more robust 
  (circuit breaker, retries, etc.)
* Much more verification and error handling (empty file, wrong file extension, unsupported 
  conversion format, etc.)
* Better API doc (OpenAPI/Swagger)
* More 2e2 tests
* More integration tests
* More unit tests
* Producing metrics
* More logs
* Terraform scripts for cluod deployment
* CI/CD pipeline